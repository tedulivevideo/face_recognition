# 人脸识别

## 项目介绍
1、本课程是达内直播课推出的全新人工智能之人脸识别的微课，后续会推出系列微课程。
2、课程基于百度AI开放平台通过java代码实现人脸识别技术。
3、课程视频步骤详细简单易上手，适合零基础或对编程开发感兴趣的人群学习。
4、录播视频和源码均提供免费下载。1、本课

## 课程目录

1、注册百度开放平台账号。
2、创建应用获取API Key和Secret Key
3、下载SDK软件开发工具包。
4、创建更改功能类。
5、应用创建好的功能类。